const footer = document.querySelector(".footer");
const currentYear = new Date().getFullYear();

function Footer() {
    const footerText = `
        <div class="text-center p-3">
            ${currentYear} &copy; <span class="copyright">Copyright:</span>
            <a href="https://dan-it.com.ua/" style="color: black">Dan-It-School</a>
        </div>
    `;
    footer.innerHTML = footerText;
}

export default Footer;
