// import map from "./js/map.js";
import Footer from "./js/footer.js";



const inputIp = document.getElementById("basic-url");
const resultField = document.querySelector(".result-field");
const exampleId = document.getElementById("basic-addon3");
const applyBtn = document.querySelector(".btn-light");
const resetBtn = document.querySelector(".btn-outline-dark");
const warningMsg = document.getElementById("basic-addon4");

// Перевірка введеного поля
inputIp.addEventListener("input", function (event) {
    const inputValue = event.target.value;
    const validCharacters = /^[0-9.]*$/;

    if (!validCharacters.test(inputValue)) {

        changeWarning();
        event.target.value = inputValue.replace(/[^0-9.]/g, "");
    }
})
// Попередження некоректного введення
function changeWarning() {
    const defaultDescription = "Range of numbers 0..9 Groups by 1-3 Divided by dot";
    warningMsg.textContent = "IP address contain (0-9) NUMBERS and DOT only!";
    warningMsg.style.cssText = "color: yellow";
    setTimeout(() => {warningMsg.textContent = defaultDescription; warningMsg.style.cssText = "color:default"}, 3000);
}

// Отримання обєкту данних з API
async function fetchData(ip) {
    try {
        const result = await fetch(`http://ip-api.com/json/${ip}`);
        const data = await result.json();
        return data;
    } catch (error) {
        console.error("Error fetching or displaying data:", error);
    }
}

// Значення за замовчуванням у випадку пустого поля IP (витягує поточну адресу)
async function getDefault() {
    try {
        const result = await fetchData("");
        exampleId.textContent = `IP: ${result.query}`;
    } catch (error) {
        console.error("Error fetching or displaying data:", error);
        exampleId.textContent = "IP: ---.---.---.---";
    }
}
// Відображення результатів
async function displayData() {
    const value = inputIp.value || "";
    const data = await fetchData(value);

    let htmlContent = `<h5>${data.query|| ''}</h5><hr>`;
    htmlContent += `<p class="out-fields">${data.continent || ''}</p>`;
    htmlContent += `<p class="out-fields"><span class="out-default-item">Country</span> ${data.country || ''}</p>`;
    htmlContent += `<p class="out-fields"><span class="out-default-item">Region</span> ${data.regionName || ''}</p>`;
    htmlContent += `<p class="out-fields"><span class="out-default-item">City</span> ${data.city || ''}</p>`;
    htmlContent += `<p class="out-fields"><span class="out-default-item">ZipCode</span> ${data.zip || ''}</p>`;
    htmlContent += `<p class="out-fields"><span class="out-default-item">Provider</span> ${data.org || ''}</p>`;

    if (data.continent || data.country || data.regionName || data.city || data.zip) {
        htmlContent += `<a href="https://www.google.com/maps/search/?api=1&query=${data.lat},${data.lon}" class="link-info">Map</a>`;
    }

    resultField.innerHTML = htmlContent;
}

// Очистка полів
applyBtn.addEventListener("click", displayData);
resetBtn.addEventListener("click", () => {
    resultField.textContent = "";
    inputIp.value = "";
})


getDefault();
Footer();
